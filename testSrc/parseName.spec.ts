import { test } from "@b08/test-runner";
import { getCurrentPackageName, PackageName, parseName } from "../src";

test("full package name", t => {
  // arrange
  const pkg = { name: "@b08/array" };
  const expected: PackageName = {
    accountName: "b08",
    packageName: "array"
  };

  // act
  const result = parseName(pkg);

  // assert
  t.deepEqual(result, expected);
});

test("package name without account", t => {
  // arrange
  const pkg = { name: "array" };
  const expected: PackageName = {
    accountName: null,
    packageName: "array"
  };

  // act
  const result = parseName(pkg);

  // assert
  t.deepEqual(result, expected);
});

test("current name", t => {
  // arrange
  // warning: this is a non-deterministic test
  const expected: PackageName = {
    accountName: "b08",
    packageName: "current-package-name"
  };

  // act
  const result = getCurrentPackageName();

  // assert
  t.deepEqual(result, expected);
});
