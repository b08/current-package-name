import { PackageName } from "./packageName.type";
import fs from "fs";
import { parseName } from "./parseName";

export function getCurrentPackageName(): PackageName {
  const file = fs.readFileSync("./package.json", "utf-8");
  return parseName(JSON.parse(file));
}
