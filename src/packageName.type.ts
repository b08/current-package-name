export interface PackageName {
  accountName: string;
  packageName: string;
}
