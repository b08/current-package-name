import { PackageName } from "./packageName.type";

export function parseName(pkg: { name: string }): PackageName {
  const names = pkg.name.split("/");
  const account = names.length === 2 ? names[0].slice(1) : null;
  const name = names[names.length - 1];
  return { accountName: account, packageName: name };
}
